
package com.neil.android.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 需要修改自己的jdk为1.8
 * @author Administrator
 * @site https://jsper.oschina.io/
 *
 */
public class AutoGetUrlAndAnalyse {
	public static void main(String[] args) {

		String oldLink2 = "http://www.80s.la/movie/";
		/**
		 * oldLink2可以使用一下网址
		 * http://www.80s.la/ju/
		 * http://www.80s.la/movie/
		 * http://www.80s.la/dm/
		 * http://www.80s.la/mv/
		 * http://www.80s.la/video/
		 * http://www.80s.la/course/
		 */
		
		//创建一个大小为100的线程池
		ExecutorService pool = Executors.newFixedThreadPool(100);
		for (int i = 1; i < 50000; i++) {
			final int index = i;
			Runnable runnable = () -> {

				try {
					StringBuilder stringBuilder = new StringBuilder();
					System.out.println(oldLink2 + index);
					Document doc = Jsoup.parse(executeGet(oldLink2 + index));
					// System.out.println(doc.data());
					// 获取标题，并添加到sb中
					Element title = doc.select("meta[property=og:title]").first();
					stringBuilder.append(title.attr("content")).append(System.getProperty("line.separator"));
					// 获取描述，og:description
					Element desc = doc.select("meta[property=og:description]").first();
					stringBuilder.append(desc.attr("content")).append(System.getProperty("line.separator"));

					// 获取海报
					Element image = doc.select("meta[property=og:image]").first();
					stringBuilder.append(image.attr("content")).append(System.getProperty("line.separator"));
					/*注释的代码为获取单个的下载地址*/
//					// 获取迅雷地址
//					Element linkchild = doc.getElementsByAttributeValue("class", "fa fa-download").first();
//					Element link = linkchild.parent();
//					stringBuilder.append(link.attr("href")).append(System.getProperty("line.separator"));
					
					//获取所有的下载地址,所有的下载链接都在dllist1这个ul中
					Elements liTag = doc.getElementsByClass("dllist1").first().children();
					
				
					//去掉第一个和最后一个li，这两个li不是下载地址所在的li
					for(int idx=1;idx<liTag.size()-1;idx++)
					{
						Element el =liTag.get(idx);
						try{
						//采用http方式的下载
						Element videoName= el.getElementsByClass("dlname nm").first().getElementsByTag("span").first();
						Element input4mp4 = el.getElementsByClass("dlname nm").first().getElementsByTag("input").first();

						//迅雷下载链接2
						//Element xunlei2= el.getElementsByClass("dlbutton2").first().getElementsByTag("a").first();
						//Element xunlei2= el.child(2).child(0);
						
						//迅雷下载链接1
						//Element xunlei1= el.getElementsByClass("dlbutton1").first().getElementsByTag("a").first();
						Element xunlei1= el.child(1).child(0);
						
						stringBuilder.append(videoName.text()+":").append(System.getProperty("line.separator"));;
						stringBuilder.append("http下载地址："+input4mp4.val()).append(System.getProperty("line.separator"));
						//stringBuilder.append("迅雷下载地址2："+xunlei2.attr("href")).append(System.getProperty("line.separator"));
						stringBuilder.append("迅雷下载地址1："+xunlei1.attr("href")).append(System.getProperty("line.separator"));
						}catch(Exception ex)
						{
							System.err.println(ex.getMessage());
						}
					}

					System.out.println(stringBuilder.toString());
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			};
			pool.execute(runnable);
		}
	}

	@SuppressWarnings("finally")
	public static String executeGet(String url) throws Exception {
		BufferedReader in = null;

		String content = null;
		try {
			// 定义HttpClient
			HttpClient client = new DefaultHttpClient();
			// 实例化HTTP方法
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);

			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			content = sb.toString();
		} finally {
			if (in != null) {
				try {
					in.close();// 最后要关闭BufferedReader
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return content;
		}
	}
}