package com.neil.android.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

@SuppressWarnings("deprecation")
public class GetVideoLinkByJsoup {

	public static void main(String[] args) {
		String oldLink2 = "http://www.80s.la/movie/";
		ExecutorService pool = Executors.newFixedThreadPool(100);
		for (int i = 10000; i < 50000; i++) {
			final int index = i;
			Runnable runnable = () -> {

				try {
					StringBuilder stringBuilder = new StringBuilder();
					System.out.println(oldLink2 + index);
					Document doc = Jsoup.parse(executeGet(oldLink2 + index));
					// System.out.println(doc.data());
					// 获取标题，并添加到sb中
					Element title = doc.select("meta[property=og:title]").first();
					stringBuilder.append(title.attr("content")).append(System.getProperty("line.separator"));
					// 获取描述，og:description
					Element desc = doc.select("meta[property=og:description]").first();
					stringBuilder.append(desc.attr("content")).append(System.getProperty("line.separator"));

					// 获取海报
					Element image = doc.select("meta[property=og:image]").first();
					stringBuilder.append(image.attr("content")).append(System.getProperty("line.separator"));
					// 获取迅雷地址
					Element linkchild = doc.getElementsByAttributeValue("class", "fa fa-download").first();
					Element link = linkchild.parent();
					stringBuilder.append(link.attr("href")).append(System.getProperty("line.separator"));
					;

					System.out.println(stringBuilder.toString());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			};
			pool.execute(runnable);
		}
	}

	@SuppressWarnings("finally")
	public static String executeGet(String url) throws Exception {
		BufferedReader in = null;

		String content = null;
		try {
			// 定义HttpClient
			HttpClient client = new DefaultHttpClient();
			// 实例化HTTP方法
			HttpGet request = new HttpGet();
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);

			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			content = sb.toString();
		} finally {
			if (in != null) {
				try {
					in.close();// 最后要关闭BufferedReader
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return content;
		}
	}

	/**
	 * 将获取到的数据保存在数据库中
	 * 
	 * @param baseUrl
	 *            爬虫起点
	 * @return null
	 */
	public void saveData(String baseUrl) {
		Map<String, Boolean> oldMap = new LinkedHashMap<String, Boolean>(); // 存储链接-是否被遍历

		Map<String, String> videoLinkMap = new LinkedHashMap<String, String>(); // 视频下载链接
		String oldLinkHost = ""; // host

		Pattern p = Pattern.compile("(https?://)?[^/\\s]*"); // 比如：http://www.zifangsky.cn
		Matcher m = p.matcher(baseUrl);
		if (m.find()) {
			oldLinkHost = m.group();
		}

		oldMap.put(baseUrl, false);
		videoLinkMap = crawlLinks(oldLinkHost, oldMap);
		// 遍历，然后将数据保存在数据库中

		// for (Map.Entry<String, String> mapping : videoLinkMap.entrySet()) {
		//
		// String res = String.format("insert into movie(MovieName,MovieLink)
		// values(%s,%s)", mapping.getKey(),
		// mapping.getValue());
		// System.out.println(res);
		// }

	}

	/**
	 * 抓取一个网站所有可以抓取的网页链接，在思路上使用了广度优先算法 对未遍历过的新链接不断发起GET请求， 一直到遍历完整个集合都没能发现新的链接
	 * 则表示不能发现新的链接了，任务结束
	 * 
	 * 对一个链接发起请求时，对该网页用正则查找我们所需要的视频链接，找到后存入集合videoLinkMap
	 * 
	 * @param oldLinkHost
	 *            域名，如：http://www.zifangsky.cn
	 * @param oldMap
	 *            待遍历的链接集合
	 * 
	 * @return 返回所有抓取到的视频下载链接集合
	 */
	private Map<String, String> crawlLinks(String oldLinkHost, Map<String, Boolean> oldMap) {
		Map<String, Boolean> newMap = new LinkedHashMap<String, Boolean>(); // 每次循环获取到的新链接
		Map<String, String> videoLinkMap = new LinkedHashMap<String, String>(); // 视频下载链接
		String oldLink = "";

		for (Map.Entry<String, Boolean> mapping : oldMap.entrySet()) {
			// System.out.println("link:" + mapping.getKey() + "--------check:"
			// + mapping.getValue());
			// 如果没有被遍历过
			if (!mapping.getValue()) {
				oldLink = mapping.getKey();
				// 发起GET请求
				try {
					URL url = new URL(oldLink);
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("GET");
					connection.setConnectTimeout(25000);
					connection.setReadTimeout(25000);

					if (connection.getResponseCode() == 200) {
						InputStream inputStream = connection.getInputStream();
						BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
						String line = "";
						Pattern pattern = null;
						Matcher matcher = null;
						// 电影详情页面，取出其中的视频下载链接，不继续深入抓取其他页面
						if (isMoviePage(oldLink)) {
							try {
								StringBuilder stringBuilder = new StringBuilder();
								Document doc = Jsoup.connect("oldLink").userAgent("Mozilla").cookie("auth", "token")
										.timeout(3000).get();
								// 获取标题，并添加到sb中
								Element title = doc.select("meta[property=og:title]").first();
								stringBuilder.append(title.attr("content"));
								// 获取描述，og:description
								Element desc = doc.select("meta[property=og:description]").first();
								stringBuilder.append(desc.attr("content"));

								// 获取海报
								Element image = doc.select("meta[property=og:image]").first();
								stringBuilder.append(image.attr("content"));
								// 获取迅雷地址
								Element link = doc.select("a[rel=nofollow]").first();
								stringBuilder.append(link.attr("href"));

								System.out.println(stringBuilder.toString());
							} catch (Exception e) {
								continue;
							}
						}
						// 电影列表页面
						else if (checkUrl(oldLink)) {
							while ((line = reader.readLine()) != null) {

								pattern = Pattern.compile("<a href=\"([^\"\\s]*)\"");
								matcher = pattern.matcher(line);
								while (matcher.find()) {
									String newLink = matcher.group(1).trim(); // 链接
									// 判断获取到的链接是否以http开头
									if (!newLink.startsWith("http")) {
										if (newLink.startsWith("/"))
											newLink = oldLinkHost + newLink;
										else
											newLink = oldLinkHost + "/" + newLink;
									}
									// 去除链接末尾的 /
									if (newLink.endsWith("/"))
										newLink = newLink.substring(0, newLink.length() - 1);
									// 去重，并且丢弃其他网站的链接
									if (!oldMap.containsKey(newLink) && !newMap.containsKey(newLink)
											&& (checkUrl(newLink) || isMoviePage(newLink))) {
										System.out.println("temp: " + newLink);
										newMap.put(newLink, false);
									}
								}
							}
						}

						reader.close();
						inputStream.close();
					}
					connection.disconnect();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				oldMap.replace(oldLink, false, true);
			}
		}
		// 有新链接，继续遍历
		if (!newMap.isEmpty())

		{
			oldMap.putAll(newMap);
			videoLinkMap.putAll(crawlLinks(oldLinkHost, oldMap)); // 由于Map的特性，不会导致出现重复的键值对
		}
		return videoLinkMap;
	}

	/**
	 * 判断是否是2015年的电影列表页面
	 * 
	 * @param url
	 *            待检查URL
	 * @return 状态
	 */
	public boolean checkUrl(String url) {
		Pattern pattern = Pattern.compile("http://www.80s.la/movie/list/-2015----p\\d*");
		Matcher matcher = pattern.matcher(url);
		if (matcher.find())
			return true; // 2015年的列表
		else
			return false;
	}

	/**
	 * 判断页面是否是电影详情页面
	 * 
	 * @param url
	 *            页面链接
	 * @return 状态
	 */
	public boolean isMoviePage(String url) {
		Pattern pattern = Pattern.compile("http://www.80s.la/movie/\\d+");
		Matcher matcher = pattern.matcher(url);
		if (matcher.find())
			return true; // 电影页面
		else
			return false;
	}
}
