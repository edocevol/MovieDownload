# 项目参考链接
1. [jsoup的使用](http://www.open-open.com/jsoup)
2. [maven依赖查找](http://mvnrepository.com/)
3. [使用httpclient获取相应字符串](http://rainbowdesert.iteye.com/blog/603327)

# 网页分析

我们使用正在播出的电视剧[《暗战危城》](http://www.80s.la/ju/18920)的页面来说明如何获取页面信息
如下图所示，在浏览器的`开发者工具`中，我们审查`《暗战危城》`页面的元素的时候，可以看到其head标签中含有大量的`meta`元素，我们只要能够提出出`meta`的信息就可以获取到电视剧的剧名、简介、海报等信息。

![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/130059_e54547b3_461425.png "在这里输入图片标题")


如下图所示，页面中还包含了`最近更新：`,`更新周期：`,`演员：`等信息。

![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/130324_b1386c7b_461425.png "在这里输入图片标题")

下图红色箭头的部分都是可以点击，调用浏览器或者迅雷进行下载的UI按钮。

![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/130503_ca5d272d_461425.png "在这里输入图片标题")

审查上面图中的`第18集的所在的li的元素`，截图如下
![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/130616_af397d28_461425.png "在这里输入图片标题")

可以看到，只要我们根据li提取出第一个span标签下的input标签和后面两个span标签的a标签就可以获取到http下载路径和迅雷下载路径

# 关于多线程的使用

```java
ExecutorService pool = Executors.newFixedThreadPool(100);
Runnable runnable = () -> {

    //线程要进行的操作
};
//线程池调度线程
pool.execute(runnable);
```

上面的代码创建了一个大小为100的线程池，线程池自动调用线程，并维持线程的声明周期。

# 项目对应的POM
```
<!-- https://mvnrepository.com/artifact/org.jsoup/jsoup -->
<dependency>
	<groupId>org.jsoup</groupId>
	<artifactId>jsoup</artifactId>
	<version>1.10.1</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient -->
<dependency>
	<groupId>org.apache.httpcomponents</groupId>
	<artifactId>httpclient</artifactId>
	<version>4.5.2</version>
</dependency>
		    
```

# 程序运行结果
爬取电视剧页面
![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/131340_f290bb56_461425.png "在这里输入图片标题")

爬取电影页面
![输入图片说明](http://git.oschina.net/uploads/images/2016/1031/131422_1b2004cc_461425.png "在这里输入图片标题")

